package cn.flowback.code.protocol.codec;

import com.alibaba.fastjson.JSONObject;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;


/**
 * 编码器
 * @author 唐警威
 **/
public class ByteEncoder extends MessageToByteEncoder<Object> {

    private ByteCompression byteCompression;

    public ByteEncoder(ByteCompression byteCompression){
        this.byteCompression = byteCompression;
    }

    @Override
    protected void encode(ChannelHandlerContext ctx, Object msg, ByteBuf out) throws Exception {
        try {
            byte[] body;
            if(msg instanceof  byte []){
                body = (byte []) msg;
            }else{
                String s = JSONObject.toJSONString(msg);
                body = byteCompression.compression(s.getBytes());
            }
            //读取消息的长度
            int dataLength = body.length;
            //先将消息长度写入，也就是消息头
            out.writeIntLE(dataLength);
            //消息体中包含我们要发送的数据
            out.writeBytes(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}