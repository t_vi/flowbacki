package cn.flowback.code.protocol.codec;

/**
 * 字节压缩/解压
 */
public interface ByteCompression {

    /**
     * 压缩字节
     * @param bytes
     * @return
     */
    byte [] compression(byte [] bytes);


    /**
     * 解压字节
     * @param bytes
     * @return
     */
    byte [] decompress(byte [] bytes);

}
