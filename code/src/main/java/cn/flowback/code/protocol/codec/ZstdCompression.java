package cn.flowback.code.protocol.codec;

import com.github.luben.zstd.Zstd;

/**
 * zstd压缩
 * @author Tang
 */
public class ZstdCompression implements ByteCompression {

    @Override
    public byte[] compression(byte[] bytes) {
        return Zstd.compress(bytes);
    }

    @Override
    public byte[] decompress(byte[] bytes) {
        int size = (int) Zstd.decompressedSize(bytes);
        byte[] ob = new byte[size];
        Zstd.decompress(ob, bytes);
        return  ob;
    }
}
