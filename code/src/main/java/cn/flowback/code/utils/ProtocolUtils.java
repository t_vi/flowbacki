package cn.flowback.code.utils;

import cn.flowback.code.protocol.BaseProtocol;
import io.netty.channel.ChannelHandlerContext;

/**
 * 协议工具
 *
 * @author 唐警威
 **/
public class ProtocolUtils {

    /**
     * 发送消息,返回是否发送成功
     * @param ctx
     * @param msg
     * @return
     */
    public static void sendMsg(ChannelHandlerContext ctx, BaseProtocol msg) {
        if (ctx != null && ctx.channel().isWritable()) {
            ctx.writeAndFlush(msg);
        }
    }
}
