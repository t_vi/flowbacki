package cn.flowback.work.controller;

import cn.flowback.client.netty.FbClient;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * 客户端处理消息器
 *
 * @author 唐警威
 **/
@RestController
public class WorkController {

    static Logger logger = LoggerFactory.getLogger(WorkController.class.getName());

    ExecutorService executorService = Executors.newWorkStealingPool(1);

    @RequestMapping("/logTestWork")
    public void logTestWork(Integer count) {
        try {
                logger.info("====开始执行发送日志任务====");
                executorService.submit(()->{
                    for (int i = 0; i < count; i++) {
                        String s = UUID.randomUUID().toString();
                        JSONObject object = new JSONObject();
                        if(i % 2 == 0 && false){
                            object.put("table","test_work");
                        }else{
                            object.put("table","test_work_2");
                        }
                        object.put("c11",s);
                        object.put("c22",s);
                        object.put("c33",s);
                        object.put("c44",s);
                        object.put("c55",s);
                        object.put("c66",s);
                        object.put("c77",s);
                        object.put("c88",s);
                        object.put("c99",s);
                        object.put("c100",s);
                        object.put("c111",s);
                        object.put("c121",s);
                        object.put("order",i);
                        if(i > 100){
                            object.put("new_columns",100);
                        }
                        FbClient.send(object);
                        //每累计发送10W条就休息下啦
                        if(i % 200000 == 0){
                            try {
                                Thread.sleep(500);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
        }catch (Exception e){
            e.printStackTrace();
        }
    }


}
