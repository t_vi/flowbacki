package cn.flowback.client.netty;

import cn.flowback.code.protocol.codec.ByteCompression;
import cn.flowback.code.protocol.codec.ByteDecode;
import cn.flowback.code.protocol.codec.ByteEncoder;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;


/**
 * 客户端处理器初始化
 * @author 唐警威
 **/
public class ClientHandlersInitializer extends ChannelInitializer<SocketChannel> {


    private TcpClient tcpClient;

    private ReconnectHandler reconnectHandler;

    private ByteCompression byteCompression;


    public ClientHandlersInitializer(TcpClient tcpClient,ByteCompression byteCompression) {
        this.tcpClient = tcpClient;
        reconnectHandler = new ReconnectHandler(tcpClient);
        this.byteCompression = byteCompression;
    }

    @Override
    protected void initChannel(SocketChannel ch){
        ChannelPipeline pipeline = ch.pipeline();
        //流量整形
        //GlobalTrafficShapingHandler globalTrafficShapingHandler = new GlobalTrafficShapingHandler(ch.eventLoop().parent(), 1 * 1024 * 1024, 10 * 1024 * 1024);
        //pipeline.addLast(globalTrafficShapingHandler);
        pipeline.addLast(reconnectHandler);
        pipeline.addLast(new ByteDecode());
        pipeline.addLast(new ByteEncoder(byteCompression));
        pipeline.addLast(new Ping());
        pipeline.addLast(new WorkClientHandler());
    }
}
