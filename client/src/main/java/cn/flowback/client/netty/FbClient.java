package cn.flowback.client.netty;

import cn.flowback.client.cache.FbServerCache;
import cn.flowback.client.callback.Message;
import cn.flowback.client.callback.SendMessageCallback;
import cn.flowback.client.config.FlowbackClientProperties;
import cn.flowback.code.protocol.codec.ByteCompression;
import cn.flowback.code.utils.ProtocolUtils;
import com.alibaba.fastjson.JSONObject;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;

import java.util.Map;
import java.util.Queue;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

public class FbClient {

    static Logger logger = Logger.getLogger(ProtocolUtils.class.getName());

    static ScheduledExecutorService executorService = Executors.newScheduledThreadPool(1);

    static AtomicInteger SEND_MESSAGE_SUCCESS_COUNT = new AtomicInteger(0);

    static  AtomicInteger SEND_FAULT_MESSAGE_COUNT = new AtomicInteger(0);

    static {
        executorService.scheduleAtFixedRate(()->{
            logger.info("Send message:"+ SEND_MESSAGE_SUCCESS_COUNT.get() +" failt: "+ SEND_FAULT_MESSAGE_COUNT.get());
        },1, 5, TimeUnit.SECONDS);
    }

    /**
     * 发送失败缓存
     */
    static Queue SEND_FAILURE_CACHE;

    static  ByteCompression byteCompression;

    public FbClient(FlowbackClientProperties flowbackClientProperties,ByteCompression byteCompression){
        SEND_FAILURE_CACHE = new LinkedBlockingQueue(flowbackClientProperties.getDefaultCacheFailureMessageCount());
        FbClient.byteCompression = byteCompression;
    }


    public static void send(Map object){
        try {
            sendMsg(FbServerCache.getFirstCtx(),
                    byteCompression.compression(new JSONObject(object).toJSONString().getBytes()),null);
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    public static void send(JSONObject object){
        try {
            sendMsg(FbServerCache.getFirstCtx(),
                    byteCompression.compression(object.toJSONString().getBytes()),null);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void send(JSONObject object, SendMessageCallback callback){
        try {
           sendMsg(FbServerCache.getFirstCtx(),
                    byteCompression.compression(object.toJSONString().getBytes()),callback);
        }catch (Exception e){
            e.printStackTrace();
        }
    }




    public static void send(ChannelHandlerContext ctx,byte [] bytes){
        try {
            sendMsg(ctx,bytes,null);
        }catch (Exception e){
            e.printStackTrace();
        }
    }



    public static void send(ChannelHandlerContext ctx,JSONObject object){
        try {
            sendMsg(ctx,byteCompression.compression(object.toJSONString().getBytes()),null);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * 发送消息,返回是否发送成功
     * @param ctx
     * @param bytes 字节数组
     * @param sendMessageCallback  发送后回调
     * @return
     */
    public static void sendMsg(ChannelHandlerContext ctx, byte [] bytes ,SendMessageCallback sendMessageCallback) {
        try {
            if (ctx != null && ctx.channel().isWritable()) {
                ChannelFuture channelFuture = ctx.writeAndFlush(bytes);
                channelFuture.addListener((future) -> {
                    if (!future.isSuccess()) {
                        SEND_FAULT_MESSAGE_COUNT.incrementAndGet();
                        if (sendMessageCallback != null) {
                            sendMessageCallback.callback(
                                    new Message.Building(bytes).setSuccess(false).build()
                            );
                        }
                    } else {
                        SEND_MESSAGE_SUCCESS_COUNT.incrementAndGet();
                        if (sendMessageCallback != null) {
                            sendMessageCallback.callback(
                                    new Message.Building(bytes).setSuccess(true).build()
                            );
                        }
                    }
                });
            } else {
                if (sendMessageCallback != null) {
                    sendMessageCallback.callback(new Message.Building(bytes).setSuccess(false).build());
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }


}
