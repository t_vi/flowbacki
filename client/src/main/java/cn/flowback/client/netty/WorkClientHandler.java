package cn.flowback.client.netty;

import cn.flowback.client.cache.FbServerCache;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 客户端处理消息器
 *
 * @author 唐警威
 **/
public class WorkClientHandler extends ChannelInboundHandlerAdapter {

    public static ChannelHandlerContext serverChanelHandler;

    static Logger logger = LoggerFactory.getLogger(WorkClientHandler.class.getName());

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
        FbServerCache.register(ctx);
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) {
        FbServerCache.register(ctx);
        serverChanelHandler = ctx;
    }


    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        logger.info("断开连接-频道不活动{}", ctx.channel().remoteAddress());
        FbServerCache.remove(ctx);
        serverChanelHandler = null;
        ctx.close();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        FbServerCache.remove(ctx);
        serverChanelHandler = null;
        cause.printStackTrace();
        logger.info("断开连接,{}", ctx.channel().remoteAddress());
        ctx.close();
    }


}
