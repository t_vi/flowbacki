package cn.flowback.client.netty;

import cn.flowback.client.config.FlowbackClientProperties;
import cn.flowback.code.protocol.codec.ByteCompression;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import java.util.logging.Logger;

/**
 * @author 唐警威
 **/
public class TcpClient {

    static Logger logger = Logger.getLogger(TcpClient.class.getName());

    Bootstrap bootstrap;

    private String ip;

    private Integer port;

    private  ByteCompression byteCompression;

    public TcpClient(FlowbackClientProperties flowbackClientProperties,  ByteCompression byteCompression) {
        this.ip = flowbackClientProperties.getIp();
        this.port = flowbackClientProperties.getPort();
        if(ip == null || port == null){
            logger.info("=================Flowback Client 未正确配置连接地址=================");
            return;
        }
        this.byteCompression = byteCompression;
        this.initBootstrap();
        this.connect();
    }

    private void initBootstrap() {
        // bootstrap 可重用, 只需在TcpClient实例化的时候初始化即可.
        EventLoopGroup group = new NioEventLoopGroup();
        bootstrap = new Bootstrap();
        //todo 后期修改可配置
        WriteBufferWaterMark writeBufferWaterMark = new WriteBufferWaterMark(1,100*1024*1024);
        bootstrap.group(group)
                .option(ChannelOption.SO_SNDBUF,2*1024*1024)
                .option(ChannelOption.SO_RCVBUF,2*1024*1024)
                .option(ChannelOption.WRITE_BUFFER_WATER_MARK, writeBufferWaterMark)
                .channel(NioSocketChannel.class)
                .handler(new ClientHandlersInitializer(this,byteCompression));
    }

    public void connect() {
        synchronized (bootstrap) {
            ChannelFuture future = bootstrap.connect(ip, port);
            future.addListener(new ChannelFutureListener() {
                @Override
                public void operationComplete(ChannelFuture future) throws Exception {
                    if (!future.isSuccess()) {
                        logger.info("建立连接失败:" + ip + ":" + port);
                        future.channel().pipeline().fireChannelInactive();
                    }else{
                        logger.info("建立连接:" + ip + ":" + port);
                    }
                }
            });
        }
    }


}
