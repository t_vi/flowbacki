package cn.flowback.client.config;

import cn.flowback.client.netty.FbClient;
import cn.flowback.client.netty.TcpClient;
import cn.flowback.code.protocol.codec.ByteCompression;
import cn.flowback.code.protocol.codec.ZstdCompression;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * client点自动配置类
 *
 * @author Tang
 */
@Configuration
@EnableConfigurationProperties(FlowbackClientProperties.class)
public class FlowBackClientConfiguration{

    @Bean
    @ConditionalOnMissingBean(ByteCompression.class)
    public ByteCompression byteCompression(){
        return new ZstdCompression();
    }

    @Bean
    @ConditionalOnMissingBean(TcpClient.class)
    public TcpClient createTcpClient(ByteCompression byteCompression,FlowbackClientProperties  flowbackClientProperties){
        TcpClient tcpClient = new TcpClient(flowbackClientProperties, byteCompression);
        return tcpClient;
    }

    @Bean
    public FbClient createFbClient(FlowbackClientProperties  flowbackClientProperties,ByteCompression byteCompression){
        return  new FbClient(flowbackClientProperties,byteCompression);
    }


}
