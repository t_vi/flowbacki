package cn.flowback.client.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 客户端配置类
 * @author Tang
 */
@ConfigurationProperties(prefix = "flowback.client")
public class FlowbackClientProperties {

    private String ip;

    private Integer port;

    /**
     * todo 待完善
     * 默认缓存失败消息消息
     */
    private Integer DefaultCacheFailureMessageCount = 100000;


    /**
     * todo 待完善
     * 开启失败消息持久化持久化
     */
    private boolean failureMessageDurable = false;


    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public Integer getDefaultCacheFailureMessageCount() {
        return DefaultCacheFailureMessageCount;
    }

    public void setDefaultCacheFailureMessageCount(Integer defaultCacheFailureMessageCount) {
        DefaultCacheFailureMessageCount = defaultCacheFailureMessageCount;
    }

    public boolean isFailureMessageDurable() {
        return failureMessageDurable;
    }

    public void setFailureMessageDurable(boolean failureMessageDurable) {
        this.failureMessageDurable = failureMessageDurable;
    }
}


