package cn.flowback.client.cache;

import io.netty.channel.ChannelHandlerContext;

import java.util.HashSet;
import java.util.Set;

/**
 * 连接缓存
 * @Autor Tang
 */
public class FbServerCache {


    private static Set<ChannelHandlerContext> CHANNEL_CACHE = new HashSet<>();


    public static ChannelHandlerContext getFirstCtx(){
        if(CHANNEL_CACHE.size() == 0){
            return null;
        }
        return CHANNEL_CACHE.stream().findFirst().get();
    }

    public static  synchronized void register(ChannelHandlerContext ctx){
        CHANNEL_CACHE.add(ctx);
    }

    public static  synchronized void remove(ChannelHandlerContext ctx){
        CHANNEL_CACHE.remove(ctx);
    }

}
