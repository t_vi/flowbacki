package cn.flowback.client.callback;

/**
 * 发送回调
 */
public interface SendMessageCallback {

     void callback(Message message);
}
