package cn.flowback.client.callback;

import java.io.Serializable;

public class Message implements Serializable {


    private  boolean isSuccess;

    private byte [] source;

    private  Message(){}


    private  Message(Building building){
        this.source = building.source;
        this.isSuccess = building.isSuccess;
    }


    public Message (byte [] source){
        this.source = source;
    }

    public static class Building{

        private  boolean isSuccess;

        private byte [] source;

        public Building(byte [] source){
            this.source = source;
        }

        public boolean isSuccess() {
            return isSuccess;
        }

        public Building setSuccess(boolean success) {
            isSuccess = success;
            return  this;
        }

        public byte[] getSource() {
            return source;
        }

        public Building setSource(byte[] source) {
            this.source = source;
            return this;
        }

        public Message build(){
            return new Message(this);
        }
    }

    public byte[] getSource() {
        return source;
    }

    public void setSource(byte[] source) {
        this.source = source;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public void setSuccess(boolean success) {
        isSuccess = success;
    }
}
