package cn.flowback.store;

/**
 * @Author: Lautumn
 * @Describe:
 * @Date: Create in 5:09 下午 2022/9/8
 */
public class AppendMessageResult {

    // Write Bytes
    private int wroteBytes;

    /**
     * 写入状态
     */
    private AppendMessageStatus status;

    public AppendMessageResult(int wroteBytes, AppendMessageStatus status) {
        this.wroteBytes = wroteBytes;
        this.status = status;
    }

    public int getWroteBytes() {
        return wroteBytes;
    }

    public void setWroteBytes(int wroteBytes) {
        this.wroteBytes = wroteBytes;
    }

    public AppendMessageStatus getStatus() {
        return status;
    }

    public void setStatus(AppendMessageStatus status) {
        this.status = status;
    }
}
