package cn.flowback.store;

/**
 * @Author: Lautumn
 * @Describe:
 * @Date: Create in 4:06 下午 2022/9/9
 */
public class DispatchRequest {

    private final boolean success;

    private final int msgSize;

    public DispatchRequest(boolean success, int msgSize) {
        this.success = success;
        this.msgSize = msgSize;
    }

    public boolean isSuccess() {
        return success;
    }

    public int getMsgSize() {
        return msgSize;
    }
}
