package cn.flowback.store;

import java.util.concurrent.locks.ReentrantLock;

/**
 * @Author: Lautumn
 * @Describe:
 * @Date: Create in 10:50 上午 2022/9/13
 */
public class PutMessageReentrantLock {
    private ReentrantLock putMessageNormalLock = new ReentrantLock(); // NonfairSync

    public void lock() {
        putMessageNormalLock.lock();
    }

    public void unlock() {
        putMessageNormalLock.unlock();
    }
}
