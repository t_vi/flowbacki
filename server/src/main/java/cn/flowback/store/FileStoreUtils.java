package cn.flowback.store;

import org.springframework.stereotype.Component;

/**
 * 文件存储工厂
 *
 * @author Tang
 */
@Component
public class FileStoreUtils {


    public static MappedFile getMappedFile(MappedFileQueue mappedFileQueue) {
        return mappedFileQueue.getLastMappedFile(true);
    }

}
