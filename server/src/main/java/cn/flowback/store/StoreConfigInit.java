package cn.flowback.store;

import cn.flowback.config.FlowBackProperties;
import com.google.common.base.Stopwatch;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author: Lautumn
 * @Describe:
 * @Date: Create in 12:01 下午 2022/9/13
 */
@Configuration
public class StoreConfigInit {
    @Bean
    public MappedFileQueue mappedFileQueue(FlowBackProperties flowBackProperties) {
        MappedFileQueue mappedFileQueue = new MappedFileQueue(flowBackProperties.getStore().getSize().intValue(), flowBackProperties.getStore().getStorePath());
        //mappedFileQueue.load();
        //mappedFileQueue.recover();
        mappedFileQueue.startFlush();
        return mappedFileQueue;
    }
}
