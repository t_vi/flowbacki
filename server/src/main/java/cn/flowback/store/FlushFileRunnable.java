package cn.flowback.store;

import com.google.common.base.Stopwatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FlushFileRunnable implements Runnable {
    private static final Logger log = LoggerFactory.getLogger(FlushFileRunnable.class);

    private MappedFileQueue mappedFileQueue;

    public FlushFileRunnable(MappedFileQueue mappedFileQueue) {
        this.mappedFileQueue = mappedFileQueue;
    }

    @Override
    public void run() {
        while (true) {
            Stopwatch stopwatch = Stopwatch.createStarted();
            mappedFileQueue.flush();
            log.info("flush耗时{}", stopwatch.stop());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                log.warn("flush中断{}", e);
            }
        }
    }
}
