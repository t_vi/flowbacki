package cn.flowback.store;

import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Author: Lautumn
 * @Describe:
 * @Date: Create in 4:53 下午 2022/9/8
 */
public class MappedFile {
    protected static final Logger log = LoggerFactory.getLogger(MappedFile.class);

    private static final int END_FILE_MIN_BLANK_LENGTH = 4 + 4;
    public final static int BLANK_MAGIC_CODE = 0xBBCCDDEE ^ 1880681586 + 8;
    public final static int MESSAGE_MAGIC_CODE = 0xAABBCCDD ^ 1880681586 + 8;
    /**
     * 写指针
     */
    protected final AtomicInteger writePosition = new AtomicInteger(0);

    /**
     * 已经刷盘指针
     */
    protected final AtomicInteger flushedPosition = new AtomicInteger(0);

    /**
     * 刷盘锁
     */
    private final Lock flushLock = new ReentrantLock();


    /**
     * mmap映射的对象
     */
    private MappedByteBuffer mappedByteBuffer;

    /**
     * 文件名称，如：00000000000536870912
     */
    private String fileName;

    /**
     * 文件名称获取的offset
     */
    private long fileFromOffset;

    /**
     * 文件大小
     */
    private int fileSize;

    private int forceIndex = 0;

    private FileChannel fileChannel;

    public MappedFile(final String fileName, final int fileSize) throws IOException {
        this.fileName = fileName;
        this.fileSize = fileSize;
        File file = new File(fileName);
        this.fileFromOffset = Long.parseLong(file.getName());
        // 初始化
        try {
            FileChannel rw = new RandomAccessFile(file, "rw").getChannel();
            fileChannel = rw;
            mappedByteBuffer = rw.map(FileChannel.MapMode.READ_WRITE, 0, fileSize);
        } catch (FileNotFoundException e) {
            log.error("create file channel " + this.fileName + " Failed. ", e);
            throw e;
        } catch (IOException e) {
            log.error("map file " + this.fileName + " Failed. ", e);
            throw e;
        }
    }

    public AppendMessageResult appendMessage(String message) {
        int currentPos = this.writePosition.get();
        if (currentPos < this.fileSize) {
            ByteBuffer buffer = this.mappedByteBuffer.slice();
            buffer.position(currentPos);
            AppendMessageResult result = doAppend(buffer, message, this.fileSize - currentPos);
            this.writePosition.addAndGet(result.getWroteBytes());
            return result;
        }
        log.error("MappedFile.appendMessage return null, wrotePosition: {} fileSize: {}", currentPos, this.fileSize);
        return new AppendMessageResult(0, AppendMessageStatus.UNKNOWN_ERROR);
    }

    public AppendMessageResult appendMessage(byte[] message) {
        int currentPos = this.writePosition.get();
        if (currentPos < this.fileSize) {
            ByteBuffer buffer = this.mappedByteBuffer.slice();
            buffer.position(currentPos);
            AppendMessageResult result = doAppend(buffer, message, this.fileSize - currentPos);
            this.writePosition.addAndGet(result.getWroteBytes());
            return result;
        }
        log.error("MappedFile.appendMessage return null, wrotePosition: {} fileSize: {}", currentPos, this.fileSize);
        return new AppendMessageResult(0, AppendMessageStatus.UNKNOWN_ERROR);

    }

    /**
     * 写入消息
     *
     * @param buffer
     * @param message
     * @param maxBlank 剩余最大空间数量
     * @return
     */
    private AppendMessageResult doAppend(final ByteBuffer buffer, final String message, final int maxBlank) {
        int bodyLength = message.getBytes().length;
        int totalSize = calMsgLength(bodyLength);
        if ((totalSize + END_FILE_MIN_BLANK_LENGTH) > maxBlank) {
            // 剩下最大空间不够写了，返回,需要填写完剩余空间，给isFull函数做判断
            buffer.putInt(maxBlank);
            buffer.putInt(BLANK_MAGIC_CODE);
            return new AppendMessageResult(maxBlank, AppendMessageStatus.END_OF_FILE);
        }
        buffer.putInt(totalSize);
        buffer.putInt(MESSAGE_MAGIC_CODE);
        buffer.putInt(bodyLength);
        buffer.put(message.getBytes());
        return new AppendMessageResult(totalSize, AppendMessageStatus.OK);
    }


    /**
     * 写入消息
     *
     * @param buffer
     * @param message
     * @param maxBlank 剩余最大空间数量
     * @return
     */
    private AppendMessageResult doAppend(final ByteBuffer buffer, final byte[] message, final int maxBlank) {
        int bodyLength = message.length;
        int totalSize = calMsgLength(bodyLength);
        if ((totalSize + END_FILE_MIN_BLANK_LENGTH) > maxBlank) {
            // 剩下最大空间不够写了，返回,需要填写完剩余空间，给isFull函数做判断
            buffer.putInt(maxBlank);
            buffer.putInt(BLANK_MAGIC_CODE);
            return new AppendMessageResult(maxBlank, AppendMessageStatus.END_OF_FILE);
        }
        buffer.putInt(totalSize);
        buffer.putInt(MESSAGE_MAGIC_CODE);
        buffer.putInt(bodyLength);
        buffer.put(message);
        return new AppendMessageResult(totalSize, AppendMessageStatus.OK);
    }

    private int calMsgLength(int bodyLength) {
        return 4 + // 总长度存储
                4 + // 魔数
                4 + // body长度存储
                bodyLength; // body数据
    }

    public long getFileFromOffset() {
        return this.fileFromOffset;
    }

    /**
     * 设置写指针位置
     *
     * @param pos
     */
    public void setWritePosition(int pos) {
        this.writePosition.set(pos);
    }

    public List<String> load() {
        ByteBuffer buffer = mappedByteBuffer.slice();
        buffer.position(0);
        List<String> messageList = Lists.newArrayList();
        while (true) {
            String message = getMessage(buffer);
            if (StringUtils.isBlank(message)) {
                break;
            }
            messageList.add(message);
        }
        return messageList;

    }

    public String getMessage(ByteBuffer buffer) {
        int totalSize = buffer.getInt();
        if (totalSize <= 0) {
            return "";
        }
        int msgCode = buffer.getInt();
        if (msgCode == BLANK_MAGIC_CODE) {
            return "";
        }
        int bodyLength = buffer.getInt();
        byte[] byteArr = new byte[bodyLength];
        buffer.get(byteArr, 0, bodyLength);
        return new String(byteArr);
    }

    /**
     * 刷盘操作
     *
     * @return 刷盘的数据长度
     */
    public int flush() {
        // 判断是否可以刷盘
        if (canFlush()) {
            // 加锁处理
            boolean locked = flushLock.tryLock();
            if (locked) {
                try {
                    int value = getReadPosition();
                    // 刷盘
                    this.mappedByteBuffer.force();
                    // 更新刷盘指针
                    this.flushedPosition.set(value);
                    if (isFull()) {
                        this.getFileChannel().close();
                    }
                } catch (IOException e) {
                    log.error("flush", e);
                } finally {
                    flushLock.unlock();
                }
            } else {
                // 有其他线程已经在刷盘了，可以设置下读写指针，快速返回
                this.flushedPosition.set(getReadPosition());
            }
        }


        return getFlushedPosition();
    }

    private boolean canFlush() {
        if (this.isFull()) {
            // 已经写完文件了，刷一次
            return true;
        }
        int flush = this.flushedPosition.get();
        int write = getReadPosition();
        // 写入的比刷盘的大，也可以刷
        return write > flush;


    }


    public int getFlushedPosition() {
        return flushedPosition.get();
    }

    /**
     * 获取读指针，可读的长度就是写入的长度
     *
     * @return
     */
    public int getReadPosition() {
        return writePosition.get();
    }

    public boolean isFull() {
        return writePosition.get() == this.fileSize;
    }

    public ByteBuffer sliceByteBuffer() {
        return mappedByteBuffer.slice();
    }

    public String getFileName() {
        return fileName;
    }

    public int getForceIndex() {
        return forceIndex;
    }

    public void setForceIndex(int forceIndex) {
        this.forceIndex = forceIndex;
    }

    public MappedByteBuffer getMappedByteBuffer() {
        return mappedByteBuffer;
    }

    public void setMappedByteBuffer(MappedByteBuffer mappedByteBuffer) {
        this.mappedByteBuffer = mappedByteBuffer;
    }

    public FileChannel getFileChannel() {
        return fileChannel;
    }

    public void setFileChannel(FileChannel fileChannel) {
        this.fileChannel = fileChannel;
    }
}
