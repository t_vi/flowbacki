package cn.flowback.store;

/**
 * @Author: Lautumn
 * @Describe:
 * @Date: Create in 9:51 上午 2022/9/9
 */
public enum AppendMessageStatus {
    OK,
    END_OF_FILE,
    MESSAGE_SIZE_EXCEEDED,
    PROPERTIES_SIZE_EXCEEDED,
    UNKNOWN_ERROR
}
