package cn.flowback.store;

import java.text.NumberFormat;

/**
 * @Author: Lautumn
 * @Describe:
 * @Date: Create in 3:04 下午 2022/9/9
 */
public class UtilAll {

    public static String offset2FileName(final long offset) {
        final NumberFormat nf = NumberFormat.getInstance();
        nf.setMinimumIntegerDigits(20);
        nf.setMaximumFractionDigits(0);
        nf.setGroupingUsed(false);
        return nf.format(offset);
    }
}
