package cn.flowback.store;

import cn.flowback.core.asynch.AsynchPoll;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ScheduledExecutorService;

/**
 * @Author: Lautumn
 * @Describe:
 * @Date: Create in 2:46 下午 2022/9/9
 */
public class MappedFileQueue {
    private static final Logger log = LoggerFactory.getLogger(MappedFileQueue.class);

    private final int mappedFileSize;
    private final String storePath;

    private AllocateMappedFileService allocateMappedFileService;

    /**
     * 记录刷盘的长度
     */
    private long flushedWhere = 0;


    /**
     * 读多写少，该并发容器合适
     */
    private final CopyOnWriteArrayList<MappedFile> mappedFiles = new CopyOnWriteArrayList<MappedFile>();

    public MappedFileQueue(int mappedFileSize, String storePath) {
        this.mappedFileSize = mappedFileSize;
        this.storePath = storePath;
        this.allocateMappedFileService = new AllocateMappedFileService();
    }

    public MappedFile getLastMappedFile(boolean needCreate) {
        long createOffset = -1;
        MappedFile lastMappedFile = getLastMappedFile();
        if (lastMappedFile == null) {
            createOffset = 0;
        }
        if (lastMappedFile != null && lastMappedFile.isFull()) {
            createOffset = lastMappedFile.getFileFromOffset() + this.mappedFileSize;
        }

        if (createOffset != -1 && needCreate) {
            String nextFilePath = this.storePath + File.separator + UtilAll.offset2FileName(createOffset);
            MappedFile mappedFile = null;
            if (this.allocateMappedFileService != null) {
                try {
                    mappedFile = this.allocateMappedFileService.createMappedFile(nextFilePath, this.mappedFileSize);
                } catch (IOException e) {
                    log.error("create mappedFile exception", e);
                }
            }
            if (mappedFile != null) {
                this.mappedFiles.add(mappedFile);
            }
            return mappedFile;
        }
        return lastMappedFile;
    }

    public MappedFile getLastMappedFile() {

        MappedFile mappedFileLast = null;

        while (!this.mappedFiles.isEmpty()) {
            // 这个容器会并发，所以使用循环的取，
            try {
                mappedFileLast = this.mappedFiles.get(this.mappedFiles.size() - 1);
                break;
            } catch (IndexOutOfBoundsException e) {
                //continue;
            } catch (Exception e) {
                log.error("getLastMappedFile has exception.", e);
                break;
            }
        }

        return mappedFileLast;

    }

    public CopyOnWriteArrayList<MappedFile> getMappedFiles() {
        return mappedFiles;
    }

    public boolean load() {
        // 读取目录下数据
        File dir = new File(this.storePath);
        File[] files = dir.listFiles();
        if (files == null) {
            return true;
        }
        Arrays.sort(files);
        for (File file : files) {
            if (file.length() != this.mappedFileSize) {
                log.warn(file + "\t" + file.length()
                        + " length not matched message store config value, ignore it");
                continue;
            }
            try {
                MappedFile mappedFile = new MappedFile(file.getPath(), this.mappedFileSize);
                mappedFile.setWritePosition(this.mappedFileSize);
                this.mappedFiles.add(mappedFile);
                log.info("load " + file.getPath() + " OK");
            } catch (IOException e) {
                log.error("load file " + file + " error", e);
                return false;
            }
        }
        return true;
    }

    public void recover() {
        // 获取最后三个文件
        final List<MappedFile> mappedFiles = this.getMappedFiles();
        if (mappedFiles.isEmpty()) {
            return;
        }
        // Began to recover from the last third file
        int index = mappedFiles.size() - 3;
        if (index < 0) {
            index = 0;
        }
        MappedFile mappedFile = mappedFiles.get(index);
        ByteBuffer byteBuffer = mappedFile.sliceByteBuffer();
        long processOffset = mappedFile.getFileFromOffset();
        long mappedFileOffset = 0;
        while (true) {
            DispatchRequest dispatchRequest = this.checkMessageAndReturnSize(byteBuffer);
            int size = dispatchRequest.getMsgSize();

            if (dispatchRequest.isSuccess() && size > 0) {
                mappedFileOffset += size;
            } else if (dispatchRequest.isSuccess() && size == 0) {
                // 读完了一个完整的文件
                index++;
                if (index >= mappedFiles.size()) {
                    // 没有文件了
                    break;
                } else {
                    // 重置继续读下一个文件
                    mappedFile = mappedFiles.get(index);
                    byteBuffer = mappedFile.sliceByteBuffer();
                    processOffset = mappedFile.getFileFromOffset();
                    mappedFileOffset = 0;
                }
            } else if (!dispatchRequest.isSuccess()) {
                log.info("recover physics file end, " + mappedFile.getFileName());
            }
        }
        processOffset += mappedFileOffset;
        this.truncateDirtyFiles(processOffset);
    }

    private void truncateDirtyFiles(long offset) {
        // 重置写指针
        for (MappedFile file : this.mappedFiles) {
            long fileTailOffset = file.getFileFromOffset() + this.mappedFileSize;
            if (fileTailOffset > offset) { // 要重置的指针是比offset之后的文件，之前的文件都是写满了的
                if (offset >= file.getFileFromOffset()) {
                    file.setWritePosition((int) (offset % this.mappedFileSize));
                }
            }
        }

    }

    private DispatchRequest checkMessageAndReturnSize(ByteBuffer buffer) {
        int totalSize = buffer.getInt();
        int msgCode = buffer.getInt();
        if (msgCode == MappedFile.BLANK_MAGIC_CODE) {
            return new DispatchRequest(true, 0);
        }
        int bodyLength = buffer.getInt();
        byte[] byteArr = new byte[bodyLength];
        buffer.get(byteArr, 0, bodyLength);
        return new DispatchRequest(true, totalSize);
    }

    /**
     * 刷盘操作
     *
     * @return
     */
    public boolean flush() {
        boolean result = true;
        // 找到需要刷盘的mappedFile
        MappedFile mappedFile = this.findMappedFileByOffset(this.flushedWhere);
        if (mappedFile != null) {
            int offset = mappedFile.flush();
            long where = mappedFile.getFileFromOffset() + offset;
            result = where == this.flushedWhere;
            // 更新刷到的位置
            this.flushedWhere = where;
        }
        return result;
    }

    private MappedFile findMappedFileByOffset(long offset) {
        MappedFile mappedFile = this.getFirstMappedFile();
        if (mappedFile != null) {
            // 比如1024 * 1024 = 1048576，
            // 文件命名则是 0000000000000
            //            0000001048576
            //            0000002097152
            // 2097152 / 1048576 = 2
            // 根据偏移量找到mappedFile所在的位置
            int index = (int) (offset / mappedFileSize);
            if (index < 0 || index >= this.mappedFiles.size()) {
                // 搜索文件有问题
                log.warn("findMappedFileByOffset offset request match error， offset:{}, index:{},mappedFileSize:{},mappedFiles count{}",
                        offset, index, mappedFileSize, mappedFiles.size()
                );
                return null;
            }
            return mappedFiles.get(index);
        }
        return null;
    }

    private MappedFile getFirstMappedFile() {
        MappedFile mappedFileFirst = null;

        if (!this.mappedFiles.isEmpty()) {
            try {
                mappedFileFirst = this.mappedFiles.get(0);
            } catch (IndexOutOfBoundsException e) {
            } catch (Exception e) {
                log.error("getFirstMappedFile exception.", e);
            }
        }
        return mappedFileFirst;

    }

    public void startFlush() {
        AsynchPoll.doFlushWork(new FlushFileRunnable(this));
    }
}
