package cn.flowback.store;

import cn.flowback.core.asynch.AsynchPoll;
import com.google.common.base.Stopwatch;

import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;

/**
 * @Author: Lautumn
 * @Describe:
 * @Date: Create in 3:35 下午 2022/9/9
 */
public class Test {


    static Set<MappedFile> mappedFiles = new HashSet<>();

    public static void main(String[] args) throws IOException {
        String storePath = "g:\\fbLocalData";
        int fileSize = 1024*1024*1024;
        MappedFileQueue mappedFileQueue = new MappedFileQueue(fileSize, storePath);
        mappedFileQueue.load();
        String message = UUID.randomUUID().toString();
        Stopwatch stopwatch = Stopwatch.createStarted();
        AsynchPoll.scheduleAtFixedRateWork(new AsynchForce(),0,1, TimeUnit.SECONDS);
        for (int i = 0; i < 2000000000; i++) {
            MappedFile mappedFile = mappedFileQueue.getLastMappedFile(true);
            mappedFiles.add(mappedFile);
            AppendMessageResult result = mappedFile.appendMessage(message);
            if (result.getStatus() == AppendMessageStatus.END_OF_FILE) {
                mappedFile.getMappedByteBuffer().force();
                mappedFile.getFileChannel().close();
                mappedFiles.remove(mappedFile);
                MappedFile lastMappedFile = mappedFileQueue.getLastMappedFile(true);
                mappedFiles.add(lastMappedFile);
                lastMappedFile.appendMessage(message);
            }
        }
       System.out.println(stopwatch.stop());
/*      int size = 0;
        for (MappedFile file : mappedFileQueue.getMappedFiles()) {
            List<String> load = file.load();
            size += load.size();
        }
        System.out.println("写入数量:" + size);*/
    }


    static class AsynchForce implements Runnable{
        @Override
        public void run() {
           try {
                for (MappedFile mappedFile : mappedFiles) {
                    if(mappedFile.isFull()){
                        mappedFile.getMappedByteBuffer().force();
                        mappedFile.getFileChannel().close();
                    }else{
                        mappedFile.getMappedByteBuffer().force();
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

}
