package cn.flowback.config;

import cn.flowback.code.protocol.codec.ByteCompression;
import cn.flowback.code.protocol.codec.ZstdCompression;
import cn.flowback.core.listener.netty.NettyServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(FlowBackProperties.class)
@ConditionalOnProperty(prefix = "flowback", value = "enabled", matchIfMissing = true)
public class FlowBackConfiguration {

    @Autowired
    private FlowBackProperties flowBackProperties;


    @Bean
    @ConditionalOnMissingBean(ByteCompression.class)
    public ByteCompression byteCompression(){
        return new ZstdCompression();
    }



}
