package cn.flowback.config;

public class Store {

    /**
     * 本地存储文件单个大小1GB
     */
    private Long size;

    /**
     * 存储文件路径
     */
    private String storePath;


    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public String getStorePath() {
        return storePath;
    }

    public void setStorePath(String storePath) {
        this.storePath = storePath;
    }
}
