package cn.flowback.core.data;

import cn.flowback.config.FlowBackProperties;
import cn.flowback.core.cache.OffHeapCache;
import cn.flowback.core.statistic.LeapArray;
import cn.flowback.core.statistic.Window;
import cn.flowback.store.*;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import org.caffinitas.ohc.OHCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.BlockingQueue;

/**
 * 本地文件存储 方式
 *
 * @author 唐警威
 **/
@Component
@ConditionalOnProperty(value = "flowback.datasourcePlatform", havingValue = "file")
public class FileStoreDataService implements DataService {

    Logger logger = LoggerFactory.getLogger(FileStoreDataService.class);

    @Autowired
    private FlowBackProperties flowBackProperties;

    @Autowired
    MappedFileQueue mappedFileQueue; // todo 将这个类弄成单例的，不需要每次new  由于是多个队列，每个队列是单线程的，这里多加线程的name来存储，这样可以不加锁的写入


    private final PutMessageReentrantLock putMessageLock = new PutMessageReentrantLock();

    @Override
    public void saveBatch(BlockingQueue<Object> logs, LeapArray leapArray) {
        MappedFile mappedFile = FileStoreUtils.getMappedFile(mappedFileQueue);
        long s = System.currentTimeMillis();
        int size = logs.size();
        int byteSize = 0;
        for (int i = 0; i < size; i++) {
            Object poll = logs.poll();
            byte[] message = null;
            if(poll instanceof byte []){
                message = (byte[]) logs.poll();
            }
            if(poll instanceof  Long){
                Long key = (Long)poll;
                OHCache<String, byte[]> cache = OffHeapCache.cache();
                message = cache.get(key.toString());
                cache.remove(key.toString());
            }
            if (message == null) {
                continue;
            }
            putMessageLock.lock();
            if (mappedFile.isFull()) {
                mappedFile = FileStoreUtils.getMappedFile(mappedFileQueue);
            }
            try {
                AppendMessageResult result = mappedFile.appendMessage(message);
                if (result.getStatus() == AppendMessageStatus.END_OF_FILE) {
                    MappedFile mappedFileNew = FileStoreUtils.getMappedFile(mappedFileQueue);
                    mappedFileNew.appendMessage(message);
                }
            } finally {
                putMessageLock.unlock();
            }
        }
        Window currentWindow = leapArray.getCurrentWindow();
        currentWindow.getSaveCount().addAndGet(size);
        long e = System.currentTimeMillis();
        logger.info("file缓存耗时:" + (e - s) + " byteSize:" + byteSize + " add:" + size + " , total:");
    }


}
